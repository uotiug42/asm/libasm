# libasm (42 school)

Post common core project in 42 school.

## Mandatory part

The idea here, is to rewrite the following functions in asm :

<table style="text-align:center;">
    <tr><td width="150">strlen</td><td width="150">strcpy</td><td width="150">strcmp</td></tr>
    <tr><td>write</td><td>read</td><td>strdup</td></tr>
</table>

## Bonus part

For the bonus part, we created some usefull functions to manipulate lists and an atoi_base function

The node of the list :
```c
typedef struct s_list
{
    void            *data;
    struct s_list   *next;
}   t_list;
```
The functions :

<table style="text-align:center;">
    <tr><td width="150">atoi_base</td><td width="150">list_push_front</td><td width="150">list_size</td></tr>
    <tr><td>list_sort</td><td>list_remove_if</td></tr>
</table>

### Compilation
```bash
git clone https://gitlab.com/uotiug42/asm/libasm.git
cd libasm

# mandatory part :
make
# mandatory + bonus part :
make bonus
# mandatory tester :
make _test
# mandatory + bonus tester
make _test_bonus
```
<br />

# Assembly Language Programming

Some usefull information about ASM for linux x86_64

## Registers

```bash
  00000000   00000000   00000000   00000000   00000000   0000000   00000000   00000000
|                                           |                    |          |          |
|                                           |                    |          |<---al--->|
|                                           |                    |                     |
|                                           |                    |<---------ax-------->|
|                                           |                                          |
|                                           |<------------------eax------------------->|
|                                                                                      |
|<-----------------------------------------rax---------------------------------------->|
```
<table style="text-align:center;">
    <tr><td width="120"><b>8-bit</b></td><td width="120"><b>16-bit</b></td><td width="120"><b>32-bit</b></td><td width="120"><b>64-bit</b></td></tr>
    <tr><td>al</td><td>ax</td><td>eax</td><td>rax</td></tr>
    <tr><td>bl</td><td>bx</td><td>ebx</td><td>rbx</td></tr>
    <tr><td>cl</td><td>cx</td><td>ecx</td><td>rcx</td></tr>
    <tr><td>dl</td><td>dx</td><td>edx</td><td>rdx</td></tr>
    <tr><td>sil</td><td>si</td><td>esi</td><td>rsi</td></tr>
    <tr><td>dil</td><td>di</td><td>edi</td><td>rdi</td></tr>
    <tr><td>bpl</td><td>bp</td><td>ebp</td><td>rbp</td></tr>
    <tr><td>spl</td><td>sp</td><td>esp</td><td>rsp</td></tr>
    <tr><td>r8b</td><td>r8w</td><td>r8d</td><td>r8</td></tr>
    <tr><td>r9b</td><td>r9w</td><td>r9d</td><td>r9</td></tr>
    <tr><td>r10b</td><td>r10w</td><td>r10d</td><td>r10</td></tr>
    <tr><td>r11b</td><td>r11w</td><td>r11d</td><td>r11</td></tr>
    <tr><td>r12b</td><td>r12w</td><td>r12d</td><td>r12</td></tr>
    <tr><td>r13b</td><td>r13w</td><td>r13d</td><td>r13</td></tr>
    <tr><td>r14b</td><td>r14w</td><td>r14d</td><td>r14</td></tr>
    <tr><td>r15b</td><td>r15w</td><td>r15d</td><td>r15</td></tr>
</table>

## Understanding Calling Conventions

When writing code for 64-bit Linux that integrates with a C library, you must follow the calling conventions explained in the AMD64 ABI Reference.

From left to right, pass as many parameters as will fit in registers. The order in which registers are allocated, are:

<table style="text-align:center;">
    <tr><td><b>For integers and pointers</b></td><td width="20">rdi</td><td width="20">rsi</td><td width="20">rdx</td><td width="20">rcx</td><td width="20">r8</td><td width="20">r9</td></tr>
    <tr><td><b>For floating-point (float, double)</b></td><td width="20">xmm0</td><td width="20">xmm1</td><td width="20">xmm2</td><td width="20">xmm3</td><td width="20">xmm4</td><td width="20">xmm5</td><td width="20">xmm6</td><td width="20">xmm7</td></tr>
</table>

Additional parameters are pushed on the stack, right to left, and are to be removed by the caller after the call.

After the parameters are pushed, the call instruction is made, so when the called function gets control, the return address is at [rsp], the first memory parameter is at [rsp+8], etc.

**The stack pointer rsp must be aligned to a 16-byte boundary** before making a call. Fine, but the process of making a call pushes the return address (8 bytes) on the stack, so when a function gets control, rsp is not aligned. You have to make that extra space yourself, by pushing something or subtracting 8 from rsp.

The only registers that the called function is required to preserve (the calle-save registers) are: rbp, rbx, r12, r13, r14, r15. All others are free to be changed by the called function.

The callee is also supposed to save the control bits of the XMCSR and the x87 control word, but x87 instructions are rare in 64-bit code so you probably don’t have to worry about this.

Integers are returned in rax or rdx:rax, and floating point values are returned in xmm0 or xmm1:xmm0.