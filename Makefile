# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gbrunet <gbrunet@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2024/04/17 17:11:54 by gbrunet           #+#    #+#              #
#    Updated: 2024/04/22 17:09:56 by gbrunet          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

_BLACK = \033[0;30m
_RED = \033[0;31m
_GREEN = \033[0;32m
_BLUE = \033[0;34m
_YELLOW = \033[0;33m
_PURPLE = \033[0;35m
_CYAN = \033[0;36m
_WHITE = \033[0;37m

_BOLD = \e[1m
_THIN = \e[2m

_END = \033[0m

NAME = libasm.a

NAME_BONUS = libasm_bonus.a

ASM = nasm

CC = cc

INCLUDES = includes

CFLAGS = -f elf64

SRC_DIR = sources/

OBJ_DIR = objects/

SRC = ft_strlen ft_strcpy ft_strcmp ft_write ft_read ft_strdup

BONUS = ft_atoi_base_bonus ft_list_push_front_bonus ft_list_size_bonus \
		ft_list_sort_bonus ft_list_remove_if_bonus

SRC_FILES = $(addprefix $(SRC_DIR), $(addsuffix .s, $(SRC)))

BONUS_FILES = $(addprefix $(SRC_DIR), $(addsuffix .s, $(SRC))) \
			  $(addprefix $(SRC_DIR), $(addsuffix .s, $(BONUS)))

OBJ_FILES = $(addprefix $(OBJ_DIR), $(addsuffix .o, $(SRC)))
OBJ_BONUS_FILES = $(addprefix $(OBJ_DIR), $(addsuffix .o, $(SRC))) \
				  $(addprefix $(OBJ_DIR), $(addsuffix .o, $(BONUS)))

COMPTEUR = 0

.PHONY : all clean fclean re bonus

$(OBJ_DIR)%.o : $(SRC_DIR)%.s
	$(eval COMPTEUR=$(shell echo $$(($(COMPTEUR)+1))))
	@mkdir -p $(@D)
	@printf "\e[?25l"
	@if test $(COMPTEUR) -eq 1;then \
		printf "$(_YELLOW)Compiling $(NAME) binary files...$(_END)\n\n";fi
	@printf "\033[A\33[2K\r$(_CYAN)Binary $(COMPTEUR): $@$(_END)\n"
	@$(ASM) $(CFLAGS) $< -o $@

$(NAME) : $(OBJ_FILES)
	@make info --no-print-directory
	@ar rc $(NAME) $(OBJ_FILES)
	@echo "$(_GREEN)Libasm created.$(_END)"
	@printf "\e[?25h"

all : $(NAME)

bonus : $(NAME_BONUS)

$(NAME_BONUS) : $(OBJ_BONUS_FILES)
	@make info --no-print-directory
	@ar rc $(NAME_BONUS) $(OBJ_BONUS_FILES)
	@echo "$(_GREEN)Libasm created.$(_END)"
	@printf "\e[?25h"

clean :
	@echo "$(_YELLOW)$(NAME): Clean...$(_END)"
	@$(RM) -rf $(OBJ_DIR)
	@echo "$(_GREEN)$(NAME): Binaries deleted.$(_END)"

fclean :
	@echo "$(_YELLOW)$(NAME): Full clean...$(_END)"
	@$(RM) -rf $(OBJ_DIR)
	@echo "$(_GREEN)$(NAME): Binaries deleted.$(_END)"
	@$(RM) $(NAME) $(NAME_BONUS) test test_bonus
	@echo "$(_GREEN)$(NAME) deleted.$(_END)"

re : 
	@make fclean --no-print-directory
	@make all --no-print-directory
	@printf "\e[?25h"

_test :
	@make all --no-print-directory
	@$(CC) -o test tester/tester.c libasm.a -I$(INCLUDES)
	@echo "$(_GREEN)Tester test created.$(_END)"

_test_bonus :
	@make bonus --no-print-directory
	@$(CC) -o test_bonus tester/tester_bonus.c libasm_bonus.a -I$(INCLUDES)
	@echo "$(_GREEN)Tester test_bonus created.$(_END)"

info :
	@printf "\t$(_PURPLE)╭──────────────────────────────────────╮"
	@printf "\n\t│$(_END)  👾  $(_CYAN)$(_THIN)Coded by $(_END)$(_CYAN)"
	@printf "$(_BOLD)guillaume brunet$(_END)$(_PURPLE)       │\n"
	@printf "\t│$(_END)  💬  $(_RED)$(_BOLD)Do not copy$(_END)$(_RED)$(_THIN), "
	@printf "$(_END)$(_RED)this is useless...$(_END) $(_PURPLE)│\n"
	@printf "\t╰──────────────────────────────────────╯\n$(_END)"


